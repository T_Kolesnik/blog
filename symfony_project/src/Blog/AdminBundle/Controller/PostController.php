<?php

namespace Blog\AdminBundle\Controller;

use Blog\ModelBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Blog\AdminBundle\Form\FileUploadType;
use Blog\AdminBundle\Form\PostType;
use Vich\UploaderBundle\Storage\StorageInterface;
/**
 * Post controller.
 *
 * @Route("post")
 */
class PostController extends Controller
{
    /**
     * Lists all post entities.
     *
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        
        
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('ModelBundle:Post')->findAll();
        
        
        $paginator=$this->get('knp_paginator');
        
        
        
        $result=$paginator->paginate(
            $posts,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',3)
            );
        
        

        return $this->render('AdminBundle:post:index.html.twig', array(
            'posts' => $result,
            'pagination' => $result
        ));
        
        
        
        
        
    }

    /**
     * Creates a new post entity.
     *
     * @param Request $request
     *
     * @Route("/new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(new PostType(), $post);
        $form->handleRequest($request);
        
        

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush($post);

            return $this->redirectToRoute('blog_admin_post_index', array('id' => $post->getId()));
        }

        return $this->render('AdminBundle:post:new.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a post entity.
     *
     * @param Post $post
     *
     * @Route("/{id}")
     * @Method("GET")
     */
    public function showAction(Post $post)
    {
        $deleteForm = $this->createDeleteForm($post);
        
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        
       
        
        $path = $helper->asset($post, 'imageFile');
        

        return $this->render('AdminBundle:post:show.html.twig', array(
            'post' => $post,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing post entity.
     *
     * @param Request $request
     * @param Post $post
     *
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Post $post)
    {
        
        $storage=$this->get('vich_uploader.storage');
        
       
        $handler=$this->get('vich_uploader.upload_handler');
        
        
        
        $translator=$this->get('translator');
        
        
       
       
        $deleteForm = $this->createDeleteForm($post);
        $editForm = $this->createForm(new PostType($storage,$handler,$translator), $post);
        $editForm->handleRequest($request);
        
        
        
        

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            
            
            
            $this->getDoctrine()->getManager()->flush();

             

            return $this->redirectToRoute('blog_admin_post_edit', array('id' => $post->getId()));
        }

        return $this->render('AdminBundle:post:edit.html.twig', array(
            'post' => $post,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a post entity.
     *
     * @param Request $request
     * @param Post $post
     *
     *
     * @Route("/{id}")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Post $post)
    {
        $form = $this->createDeleteForm($post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush($post);
        }

        return $this->redirectToRoute('blog_admin_post_index');
    }

    /**
     * Creates a form to delete a post entity.
     *
     *
     *
     * @param Post $post The post entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blog_admin_post_delete', array('id' => $post->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
