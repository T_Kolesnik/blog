<?php

namespace Blog\AdminBundle\Form;


use ModelBundle\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Vich\UploaderBundle\Storage\StorageInterface;
use Vich\UploaderBundle\Handler\UploadHandler;
use Symfony\Component\Translation\TranslatorInterface;

class FileUploadType extends AbstractType
{
    
    private $interface;
    private $handler;
    private $translator;
    
   public function __construct(StorageInterface $interface,UploadHandler $handler,TranslatorInterface $translator) {
       
       $this->interface=$interface;
       $this->handler=$handler;
       $this->translator=$translator;
       
   }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageName')
            ->add('imageFile', new  VichImageType($this->interface,$this->handler,$this->translator), [
                'required' => false,
                'allow_delete' => true,
                'download_link' => true,
                'mapped' => true,
                'data_class' => null
            ])
            ->add('submit', new SubmitType);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\ModelBundle\Entity\Post',
            'csrf_protection' => false,
        ));
    }
    
    
     /**
     * @return string
     */
    public function getName()
    {
        return 'blog_modelbundle_file';
    }
}