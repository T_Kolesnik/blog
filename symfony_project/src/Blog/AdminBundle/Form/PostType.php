<?php

namespace Blog\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Vich\UploaderBundle\Storage\StorageInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use  Blog\ModelBundle\Entity\Post;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Vich\UploaderBundle\Handler\UploadHandler;
use Symfony\Component\Translation\TranslatorInterface;

class PostType extends AbstractType
{
    
   
    
    
    
    
    
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {  
       
        
        
       
        
        
        $builder
            ->add('title')
            ->add('slug')
            ->add('body')
            ->add('author')
            ->add('category')
            ->add('imageFile', 'vich_image', [
            'required' => false,
            'allow_delete' => true, // not mandatory, default is true
            'download_link' => true, // not mandatory, default is true
        ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\ModelBundle\Entity\Post'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blog_modelbundle_post';
    }
}
