<?php

namespace Blog\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class AuthorController extends Controller
{
    /**
     *
     * Show a author
     *
     * @param string $slug
     *
     * @Route("/author/{slug}" )
     *
     * @Template()
     * @throws NotFoundHttpException
     * @return array()
     */
    public function showAction(Request $request,$slug)
    {
        
        
        
        $author=$this->getAuthorManager()->findBySlug($slug);
        
        
        
        
       $posts=$this->getAuthorManager()->findPosts($author);
       
       
       $paginator=$this->get('knp_paginator');
        
        
        
        $result=$paginator->paginate(
            $posts,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',3)
            );
        
        
        return  array(
            
            'author'=>$author,
            'posts'=>$result
            
            
            );
    }
   
   
    private function getAuthorManager()
    {
        return $this->get('author_manager');
    }
    
    
    
    
    
}
