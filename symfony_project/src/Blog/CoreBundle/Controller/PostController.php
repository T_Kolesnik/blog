<?php

namespace Blog\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Blog\ModelBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Blog\ModelBundle\Entity\Comment;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

//use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     * @return array()
     */
    public function indexAction()
    
    {
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        
        $em = $this->getDoctrine()->getManager();
            $reclames = $em->getRepository('ModelBundle:Reclame')->findAll();
        
        
        foreach($reclames as $reclame){
            
         $path = $helper->asset($reclame, 'imageFile');   
            
            
        }
        
        
        
        $posts_top=$this->getPostManager()->getTopPosts(6);
        
        foreach($posts_top as $post){
            
         $path = $helper->asset($post, 'imageFile');   
            
            
        }
        
       
         
        
      
        
        $categories=$this->getCategoryManager()->findCategories(3);
        
        foreach($categories as $category){
            
            foreach($category->getPosts() as $post){
                
               $path = $helper->asset($post, 'imageFile');  
            }
            
        }
        
        //$posts=$this->getPostManager()->findAll();
        
        $post_latest=$this->getPostManager()->getLatestPosts(3);
        
        //$p=$this->getPostManager()->getResultSearch('-Facilis');
        
        foreach($post_latest as $post){
            
            $path = $helper->asset($post, 'imageFile');   
            
        }
        
        
        $comment_latest=$this->getCommentManager()->getLatestComments(3);
        
        
        
        
        return  array(
            
            //'posts'=>$posts,
            
            'post_latest'=>$post_latest,
            
            'categories' =>$categories,
            
            'comment_latest' =>$comment_latest,
            
            'posts_top' =>$posts_top,
            
            'reclames'=>$reclames
            );
    }
     /**
     *
     * Show a post
     *
     * @param string $slug
     *
     * @Route("/{slug}")
     * @Template()
     * @throws NotFoundHttpException
     * @return array()
     **/
     
    public function showAction($slug)
    {
        $post=$this->getPostManager()->findBySlug($slug);
        
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $path = $helper->asset($post, 'imageFile');
         
         
        
        $comment_type=$this->get('comment.form.type');
        
        $form=$this->createForm($comment_type);
        
        $comments=$post->getComments();
        
        foreach ($comments as $comment) {
            
         $date_now=new \DateTime();
         
         $date_life_link=$comment->getLifeLinkEdit();
         
        
         
            $comment->setActiveLink($date_now<$date_life_link);
            
            
        }
       
        
        
        return  array(
            
            'post'=>$post,
            
            'form'=>$form->createView()
            
            );
    }

    /**
     * create comment
     *
     * @param Request $request
     * @param string $slug
     *
     * @Route("/{slug}/create_comment")
     * @Method("POST")
     * @Template("CoreBundle:Post:show.html.twig")
     *
     *
     **/
    
    
    public function createCommentAction(Request $request, $slug)
    {
        $post=$this->getPostManager()->findBySlug($slug);
        
        $comment_type=$this->get('comment.form.type');
        
        $form=$this->getPostManager()->createComment($post, $request ,$comment_type);
        
        
        if (true===$form) {
            $this->get('session')->getFlashBag()->add('success', 'This comment was submitted successfully');
         
            return $this->redirect($this->generateUrl('blog_core_post_show', ['slug'=>$post->getSlug()])) ;
        }
        
        
        
        return array(
            
                   'post' => $post,
                   'form' => $form
            
            );
    }

    private function getPostManager()
    {
        return $this->get('post_manager');
    }
    
    private function getCategoryManager()
    {
        return $this->get('category_manager');
    }
    
     private function getCommentManager()
    {
        return $this->get('comment_manager');
    }
     /**
      * 
      * @param string $name
      * 
     * @Route("/category/{name}")
     * @Template()
     * @return array()
     */
    public function categoryAction(Request $request,$name)
    {
        
        
        
        
        $categories_posts=$this->getCategoryManager()->findCategoriesPosts($name);
        
        
        
        $paginator=$this->get('knp_paginator');
        
        
        
        $result=$paginator->paginate(
            $categories_posts,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',3)
            );
        
       
        return  array(
            
            'posts' =>$result,
            
            'name' =>$name
            
            
            
            );
    }
    
    
    /**
     *
     * @Route("post/search")
     * 
     * 
     **/
     
    public function searchAction(Request $request)
    {
       
      
        
        if($request->getMethod()=="POST")
    {
 
 
 
           $search=$request->get('q');  
  
  

 $result=$this->getPostManager()->getResultSearch($search);
 

 
 $response = new Response();
 
 
 
 
 
 
if (count($result) > 0)
{
   
   $content="";
   
 foreach($result as $key=>$post) {  
     
    
  $content.= '
 

    <li>
      <div class ="fotosearch">
            
      <img   src="/bundles/img/'.$post->getImageName().'" />      
            </div>
      <div class="block-title-price">
        <a href="'.$post->getSlug().'">'.$post->getTitle().'</a>
      </div>
    </li>
';


if(4==$key){break;};

}


if (count($result) > 5)
{
    $content.= '
           <center>
              <a id="search-more" href="/post/search?q='.$request->get('q').'">Посмотреть все результаты →</a>
           </center>    
';
 
}


$response->setContent($content); 


 
}

else{
 

  $response->setContent( '
<center>
<a id="search-noresult">Ничего не найдено! :\'(</a>
</center>    
');    
}


return $response;

 } ;  
       

if($request->getMethod()=="GET")
    {
 
 
       $search = $request->query->get('q'); 
 
        $posts=$this->getPostManager()->getResultSearch($search);
 
        
 
 
        $paginator=$this->get('knp_paginator');
        
        
        
        $result=$paginator->paginate(
            $posts,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',3)
            );
 
 
 
             
       return $this->render('CoreBundle:Post:search.html.twig', array(
            'posts' => $result,
        )
        
        );
            
            
            
            

          
           
        
            }

    }   
    
   
   /**
     *
     * @Route("/blog/about")
     * @Template()
     * 
     * @return array()
     **/
     
    public function aboutAction()
    {
   
    
   
     
     return $this->render('CoreBundle:Post:about.html.twig');
   
    }
    
    
    
     /**
     *
     * @Route("/blog/info")
     * 
     * 
     * 
     **/
     
    public function infoAction()
    {
        
        
        
     $file=file(dirname(__DIR__).'/Resources/views/Post/info.html' );
     
     $response = new JsonResponse();
     
     $response->setData(array("data"=>$file));
     

   
        
        
     return $response;
     
   
    }
}

