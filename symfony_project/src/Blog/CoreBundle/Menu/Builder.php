<?php

namespace Blog\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function categoryMenu(FactoryInterface $factory, array $options)
    {
           	$menu = $factory->createItem('root');
       	    $menu->setChildrenAttribute('class',  'nav navbar-nav');
    	
    	
    	/*
    	You probably want to show user specific information such as the username here. That's possible! Use any of the below methods to do this.
    	
    	if($this->container->get('security.context')->isGranted(array('ROLE_ADMIN', 'ROLE_USER'))) {} // Check if the visitor has any authenticated roles
    	$username = $this->container->get('security.context')->getToken()->getUser()->getUsername(); // Get username of the current logged in user
    	
    	*/	
		$menu->addChild('User', array('label' => 'Category Posts'))
			->setAttribute('dropdown', true)
			->setAttribute('icon', 'icon-user');
		
		$em = $this->container->get('doctrine')->getManager();
		$categories = $em->getRepository('ModelBundle:Category')->findAll();
		
		
		foreach($categories as $category){
			
		$menu['User']->addChild($category->getName(), array('route' => 'blog_core_post_category' ,'routeParameters' => array('name' => $category->getName())));
		
		}             
		
			
    			
        return $menu;
}



  public function registerMenu(FactoryInterface $factory, array $options)
    {
           	$menu = $factory->createItem('root');
       	    
       	    $menu->setChildrenAttribute('class', 'nav navbar-nav');
       	    $menu->addChild('Register', array('route' => 'fos_user_registration_register'
       	                                      
       	    )); 
       	    
       	    return $menu;
    }

 public function loginMenu(FactoryInterface $factory, array $options)
    {
           	$menu = $factory->createItem('root');
       	    $menu->setChildrenAttribute('class', 'nav navbar-nav');
       	    
       	    $menu->addChild('Login', array('route' => 'blog_admin_default_index')); 
       	    
       	    return $menu;
    }
    
  public function mainMenu(FactoryInterface $factory, array $options)
    {
           	$menu = $factory->createItem('root');
       	    $menu->setChildrenAttribute('class', 'nav navbar-nav');
       	    
       	    $menu->addChild('Home', array('route' => 'blog_core_post_index')); 
       	    
       	    return $menu;
    }  

  public function aboutMenu(FactoryInterface $factory, array $options)
    {
           	$menu = $factory->createItem('root');
       	    $menu->setChildrenAttribute('class', 'nav navbar-nav');
       	    
       	    $menu->addChild('About Blog', array('route' => 'blog_core_post_about')); 
       	    
       	    return $menu;
    }  


}
    
