<?php

namespace Blog\CoreBundle\Services;

use Doctrine\ORM\EntityManager;

use Symfony\Component\HttpFoundation\Request;
use Blog\ModelBundle\Entity\Comment;
use Blog\ModelBundle\Entity\Post;
use Blog\ModelBundle\Entity\Category;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



/**
 *
 * class categoryManager
 *
 **/


class CategoryManager
{
    private $em;
    
    /**
     * construct
     * @param EntityManager $em
     *
     **/
    public function __construct(EntityManager $em)
    {
        $this->em=$em;
        
    }
   
    /**
     * 
     *
     * return Blog\ModelBundle\Entity\Categories[]
     *
     **/
     
    public function findCategories($limit)
    {
        $categories=$this->em->getRepository('ModelBundle:Category')->findAll();
        
        $result=array();
        
        foreach($categories as $temp_category){
          
          $category=new Category;
          
          $category->setId($temp_category->getId());
          $category->setName($temp_category->getName());
         
          $posts=$this->em->getRepository('ModelBundle:Category')->findByPosts($category,$limit);
          
         
          $category->setPosts($posts);
          
          $result[]=$category;
        
        }
         
        
        
        return $result;
    }
   
     public function findCategoriesPosts($name){
         
         
         
        
         
         $category=$this->em->getRepository('ModelBundle:Category')->findOneBy(['name'=>$name]);
         
        
         
         $posts=$this->em->getRepository('ModelBundle:Post')->findBy(['category'=>$category]);
         
         
         //$post=$this->em->getRepository('ModelBundle:Category')->findByPosts($category);
         
         
         
         
         return $posts;
     }
   
    
}