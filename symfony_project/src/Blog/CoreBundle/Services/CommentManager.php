<?php

namespace Blog\CoreBundle\Services;

use Doctrine\ORM\EntityManager;

use Symfony\Component\HttpFoundation\Request;
use Blog\ModelBundle\Entity\Comment;
use Blog\ModelBundle\Entity\Post;
use Blog\ModelBundle\Entity\Category;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



/**
 *
 * class commentManager
 *
 **/


class CommentManager
{
    private $em;
    
    /**
     * construct
     * @param EntityManager $em
     *
     **/
    public function __construct(EntityManager $em)
    {
        $this->em=$em;
        
    }
   
    /**
     * 
     *
     * return Blog\ModelBundle\Entity\Categories[]
     *
     **/
     
    public function getLatestComments($limit)
    {
        $comments=$this->em->getRepository('ModelBundle:Comment')->findByComments($limit);
        
       foreach($comments as $comment){
           
           $id=$comment->getPost()->getId();
           
           $post=$this->em->getRepository('ModelBundle:Post')->findOneBy(['id'=>$id]);
           
          
           
           $comment->setPost($post);
       }
         
        
        
        return $comments;
    }
    
}