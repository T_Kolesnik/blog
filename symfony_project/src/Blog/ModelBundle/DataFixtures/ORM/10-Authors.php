<?php

namespace Blog\ModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Blog\ModelBundle\Entity\Author;
use Faker\Factory as FakerFactory;

class Authors extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 10;
    }
    
    /**
     * {@inheritdoc}
     *
     **/
     
     public function load(ObjectManager $manager)
     {
         $faker=FakerFactory::create();
        
         $a0 = new Author;
         $a0->setUserName('admin');
         $a0->setPlainPassword('adminpass');
         $a0->setEmail('admin@adminka.com');
         $a0->setEnabled('true');
         $a0->setRoles(['ROLE_ADMIN']);
         $a0->setName('Admin');
        
       
       for($i=1;$i<5;$i++){
         $a= new Author;
         $faker->seed(1110+$i);
         $a->setName($faker->firstName);
         $a->setUserName($faker->userName);
         $a->setPlainPassword($faker->userName);
         $a->setEmail($faker->userName.'@adminka.com');
         $a->setEnabled('true');
         $a->setRoles(['ROLE_USER']);
         

         

         $manager->persist($a);
       }
         
        
        
         $manager->persist($a0);
         
        
         $manager->flush();
     }
}
