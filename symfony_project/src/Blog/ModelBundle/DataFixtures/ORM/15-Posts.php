<?php

namespace Blog\ModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Blog\ModelBundle\Entity\Post;
use Faker\Factory as FakerFactory;

class Posts extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 15;
    }
    
   
   
   
   
   
   
  /**
     * {@inheritdoc}
     *
     **/
     
    public function load(ObjectManager $manager)
    {
        $faker=FakerFactory::create();
        
        for($i=1;$i<30;$i++){ 
        
        $b1= new Post();
        $b1->setTitle($faker->realText($maxNbChars = 20 +rand(20,50), $indexSize = 2));
        $b1->setBody($faker->realText($maxNbChars = 200 +rand(20,50), $indexSize = 2));
        $b1->setCategory($this->getCategory($manager, 'Sport'));
        $b1->setImageName($faker->image($dir =dirname(dirname(dirname(dirname(dirname(__DIR__))))). '/web/bundles/img', $width = 640, $height = 480 ,'sports',false));
        $faker->seed(1110+rand(1,4));
        $b1->setAuthor($this->getAuthor($manager, $faker->firstName));
        
       
         
        
        $b2= new Post();
        $b2->setTitle($faker->realText($maxNbChars = 25 +rand(20,50), $indexSize = 2));
        $b2->setBody($faker->realText($maxNbChars = 250 +rand(20,50), $indexSize = 2));
        $b2->setCategory($this->getCategory($manager, 'News'));
        $b2->setImageName($faker->image($dir =dirname(dirname(dirname(dirname(dirname(__DIR__))))). '/web/bundles/img', $width = 640, $height = 480 ,'people',false));
        $faker->seed(1110+rand(1,4));
        $b2->setAuthor($this->getAuthor($manager, $faker->firstName));
        
        
        $b3= new Post();
        $b3->setTitle($faker->sentence($faker->numberBetween($min = 2, $max = 11)));
        $b3->setBody($faker->paragraph(7));
        $b3->setCategory($this->getCategory($manager, 'World'));
        $b3->setImageName($faker->image($dir =dirname(dirname(dirname(dirname(dirname(__DIR__))))). '/web/bundles/img', $width = 640, $height = 480 ,'city',false));
        $faker->seed(1110+rand(1,4));
        $b3->setAuthor($this->getAuthor($manager, $faker->firstName));
        
        
        $b4= new Post();
        $b4->setTitle($faker->sentence($faker->numberBetween($min = 2, $max = 11)));
        $b4->setBody($faker->paragraph(7));
        $b4->setCategory($this->getCategory($manager, 'Politic'));
        $b4->setImageName($faker->image($dir =dirname(dirname(dirname(dirname(dirname(__DIR__))))). '/web/bundles/img', $width = 640, $height = 480 ,'business',false));
        $faker->seed(1110+rand(1,4));
        $b4->setAuthor($this->getAuthor($manager, $faker->firstName));
        
        
        
        $manager->persist($b1);
        $manager->persist($b2);
        $manager->persist($b3);
        $manager->persist($b4);
        
        $manager->flush();
        
       }
    }
   
   
    public function getAuthor(ObjectManager $manager, $name)
    {
        return $manager->getRepository('ModelBundle:Author')->findOneBy([
         
         'name'=>$name
         
         ])  ;
    }
    
     public function getCategory(ObjectManager $manager, $name)
    {
        return $manager->getRepository('ModelBundle:Category')->findOneBy([
         
         'name'=>$name
         
         ])  ;
    }
    
    
}
