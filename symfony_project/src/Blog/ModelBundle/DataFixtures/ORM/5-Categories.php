<?php

namespace Blog\ModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Blog\ModelBundle\Entity\Category;
use Faker\Factory as FakerFactory;

class Categories extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 5;
    }
    
    /**
     * {@inheritdoc}
     *
     **/
     
     public function load(ObjectManager $manager)
     {
         $faker=FakerFactory::create();
        
         $a0 = new Category;
         $a0->setName('News');
         $manager->persist($a0);
         
         
         $a1 = new Category;
         $a1->setName('Sport');
         $manager->persist($a1);
         
         $a2 = new Category;
         $a2->setName('World');
         $manager->persist($a2);
         
         $a3 = new Category;
         $a3->setName('Politic');
         $manager->persist($a3);
    
         
        
         $manager->flush();
     }
}