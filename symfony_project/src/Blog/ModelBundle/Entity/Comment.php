<?php

namespace Blog\ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Blog\ModelBundle\Entity\Post;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="Blog\ModelBundle\Repository\CommentRepository")
 */
class Comment extends Timestampable {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=100)
     * @Assert\NotBlank()
     */
     
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     * @Assert\NotBlank()
     */
    private $body;
    
     /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="Post",inversedBy="comments")
     * @ORM\JoinColumn(name="post_id",referencedColumnName="id",nullable=false)
     * @Assert\NotBlank()
     */
    private $post;


    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="life_link_edit", type="datetime")
     */
    private $lifeLinkEdit;
    
    
    /**
     * 
     * @var bool
     * 
     **/
     private $active_link;
    
     /**
     * 
     * @var bool
     * @ORM\Column(name="active_comment", type="boolean")
     * 
     **/
     private $active_comment;
    
    public function __construct(){
        
        
       $this->lifeLinkEdit=new \DateTime('+1 hour');
        
        
    }
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Comment
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Comment
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set post
     *
     * @param \Blog\ModelBundle\Entity\Post $post
     *
     * @return Comment
     */
    public function setPost(Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Blog\ModelBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }
    
    
    /* Get post
     *
     * @return \DateTime
     */
    public function getLifeLinkEdit()
    {
        return $this->lifeLinkEdit;
    }
    
     /**
     * Set active_link
     *
     * @param bool $active_link
     *
     * @return Comment
     */
    public function setActiveLink($active_link)
    {
        $this->active_link = $active_link;

        return $this;
    }

    /**
     * Get active_link
     *
     * @return bool
     */
    public function getActiveLink()
    {
        return $this->active_link;
    }
   
    /**
     * Set active_comment
     *
     * @param bool $active_comment
     *
     * @return Comment
     */
    public function setActiveComment($active_comment)
    {
        $this->active_comment = $active_comment;

        return $this;
    }

    /**
     * Get active_comment
     *
     * @return bool
     */
    public function getActiveComment()
    {
        return $this->active_comment;
    }
   
   
    
}
