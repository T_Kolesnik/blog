<?php

namespace Blog\ModelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Form\FormEvents;
use Blog\ModelBundle\Entity\Author;

class CommentType extends AbstractType
{
    
    private $SecurityContext;
    
    
    public function __construct(SecurityContextInterface $sc){
        
        
      $this->SecurityContext=$sc;  
        
        
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author')
            ->add('body')
            ->add('active_comment')
            ->add('post', new SubmitType(), ['label'=>'blog.core.post.comment.send']);
            
            
            
            
            
            
            $builder->addEventListener(FormEvents::PRE_SET_DATA,function($event){
                
              $user=$this->SecurityContext->getToken()->getUser(); 
              
              $form=$event->getForm();
              
              
                 $form->remove('active_comment');
                 $form->add(
                    'active_comment',
                    'hidden',
                    ['attr'=>['value'=>"0"]]
                    
                    
                    );
              
              
                
              if($user instanceof Author){
                  
                $name=$user->getName();  
                
                
                $form->remove('author');
                $form->add(
                    'author',
                    'hidden',
                    ['attr'=>['value'=>$name]]
                    
                    
                    );
                $form->remove('active_comment');
                 $form->add(
                    'active_comment',
                    'hidden',
                    ['attr'=>['value'=>"0"]]
                    
                    
                    );
                 
                 }  
                
                
                
            });
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\ModelBundle\Entity\Comment'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blog_modelbundle_comment';
    }
}
